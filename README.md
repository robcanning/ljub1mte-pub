# _:ljub1mte_ [public]

## Brief Project Description

_:ljub1mte_ is an intermedia construction by Rob Canning and Monika Pocrnjić. As a work of speculative fiction _:ljub1mte_ takes as its point of departure the 'birth' of two cybersecurity AI bots. 
Through a series of machine learning interactions with one other, their immediate environment and a corpus of data-breached user credentials, the bots trawl the interwebz in an attempt to learn about human society, ethics, life, love and the universe. 
The work is created using open-source software including Supercollider, Processing and customised penetration testing scripts alongside scrap electronics and DIY robotics.

## Author Biographies

### Rob Canning
Dr Rob Canning  is an Irish musician, intermedia artist and educator.  He has produced music for the concert hall, theatre, screen and dance productions as well as working in a range of site-specific and improvisational contexts.  His compositions have been performed internationally and presented at conferences including the ICMC (International Computer Music Conference), LAC (Linux Audio Conference) and NIME ( New Interfaces for Musical Expression).
Since the late 90s, his work has relied solely on the use of FOSS (Free and Open Source Software) for the realisation and implementation of projects. As a free culture  advocate and educator he has worked closely with the GOTO10 media art collective, Openlab, London Hackspace and SPC medialab  as well as working for many years as a senior lecturer in Networked Digital Media in UK universities.
He now lives and works in Slovenia, where he and his family are developing a rural art program within the Rizoma Institute, operating at the intersection of technology, permaculture and performing arts. He holds a PhD in music composition from Goldsmiths University, London.
[http://zavodrizoma.si](http://zavodrizoma.si)

### Monika Pocrnjić
Monika Pocrnjić is an artist who works in a variety of media. By taking daily life as subject matter while commenting on the everyday aesthetic of middle class values, her artworks references post-colonial theory as well as the avant-garde or the post-modern and the d.i.y movement as a form of resistance against the logic of the capitalist market system.
Her artworks are characterised by the use of everyday objects in an atmosphere of middle class mentality in which recognition plays an important role. By choosing mainly formal solutions, she tries to develop forms that do not follow logical criteria, but are based only on subjective associations and formal parallels, which incite the viewer to make new personal associations.
Her works don't reference recognisable forms. The results are deconstructed to the extent that meaning is shifted and possible interpretation becomes multifaceted. Play is a serious matter: during the game, different rules apply than in everyday life and even everyday objects undergo transubstantiation. Her works are based on formal associations which open a unique poetic vein. Multilayered images arise in which the fragility and instability of our seemingly certain reality is questioned.
[https://github.com/monitronica](https://github.com/monitronica)

## Images

### Project Image 01
![Project Image 01](images/Ljub1mteProjectImage01.png)
### Rob Canning Profile Image
![Rob Canning Profile Image](images/RobCanning.png)
### Monika Pocrnjić Profile Image
![Monika Pocrnjić Profile Image](images/MonikaPocrnjić.png)


## Technical Requirements

_Preliminary technical rider, unconfirmed / subject to change / project in development._

- Electricity
- Cable extensions
- Monitors x 4  <= 42” / 106cm + Pillars for 2x2 video wall layout
- Projectors x 2
- Active SubWoofer x 2
- Active PA Speakers x 4 (in case of  performance version, not needed or shop window situation) 
- Audio Mixer capable of 4 channel output. e.g Mackie 16:04
- LED Wash light bar.
